package mrth.labs.java.try0;

public interface Tree<T extends Comparable<T>> {
   void insert(T key);
   boolean isEmpty();
}
