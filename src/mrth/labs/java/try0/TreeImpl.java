package mrth.labs.java.try0;

public class TreeImpl<T extends Comparable<T>> implements Tree<T> {
   Node<T> root;

   public TreeImpl() {
   }

   public TreeImpl(T key) {
      root = new Node<>(key);
   }

   @Override
   public void insert(T key) {
      root = insertRecursively(root, key);
   }

   private Node<T> insertRecursively(Node<T> root, T key) {
      if (root == null) {
         root = new Node<>(key);
         return root;
      }

      if (key.compareTo(root.getKey()) < 0)
         root.left = insertRecursively(root.left, key);
      else if (key.compareTo(root.getKey()) > 0)
         root.right = insertRecursively(root.right, key);

      return root;
   }

   public void delete(T key) {
      root = deleteRecursively(root, key);
   }

   private Node<T> deleteRecursively(Node<T> root, T key) {
      if (root == null) return root;

      if (key.compareTo(root.getKey()) < 0)
         root.left = deleteRecursively(root.left, key);
      else if (key.compareTo(root.getKey()) > 0)
         root.right = deleteRecursively(root.right, key);
      else {
         if (root.left == null)
            return root.right;
         else if (root.right == null)
            return root.left;

         root.setKey(minValue(root.right));

         root.right = deleteRecursively(root.right, root.getKey());
      }

      return root;
   }

   private T minValue(Node<T> root) {
      T minv = root.getKey();
      while (root.left != null) {
         minv = root.left.getKey();
         root = root.left;
      }
      return minv;
   }

   private String toStringAt(Node<T> node, String prefix, NodeType type) {
      StringBuilder returnString = new StringBuilder();

      if (type == NodeType.Root) {
         returnString.append(node.toString());
         returnString.append("\n");
      } else if (type == NodeType.Right) {
         returnString.append(prefix + "└<right> ");
         if (node == null) {
            returnString.append("--");
         } else {
            returnString.append(node.toString());
         }
         returnString.append("\n");
      } else {
         returnString.append(prefix + "├<left> ");
         if (node == null) {
            returnString.append("--");
         } else {
            returnString.append(node.toString());
         }
         returnString.append("\n");
      }
      String newPrefix;
      if (type == NodeType.Root) {
         newPrefix = "";
      } else {
         newPrefix = prefix + (type == NodeType.Left ? "│ " : " ");
      }

      if (node != null) {
         returnString.append(toStringAt(node.left, newPrefix, NodeType.Left));
         returnString.append(toStringAt(node.right, newPrefix, NodeType.Right));
      }

      return returnString.toString();
   }

   private enum NodeType {
      Left,
      Right,
      Root
   }

   @Override
   public boolean isEmpty() {
      return root == null;
   }

   @Override
   public String toString() {
      if (isEmpty()) {
         return "Empty";
      }
      return toStringAt(root, "", NodeType.Root);
   }
}
