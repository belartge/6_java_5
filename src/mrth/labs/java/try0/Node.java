package mrth.labs.java.try0;

public class Node<T extends Comparable<T>> {
   private T key;
   Node<T> left;
   Node<T> right;

   Node(T key) {
      this.key = key;
   }

   public T getKey() {
      return key;
   }

   public void setKey(T key) {
      this.key = key;
   }

   @Override
   public String toString() {
      return key.toString();
   }
}
